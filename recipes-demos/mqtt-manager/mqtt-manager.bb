SUMMARY = "Minimal MQTT Manager"
DESCRIPTION = "Service to handle application state in MQTT broker using libmosquitto"
AUTHOR = "alin.popa.ext@huawei.com"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=7388a7144776640b5c29ecbfa8358b44"

SRC_URI += "\
    git://gitlab.com/harmonyos-hackathon/resources-yocto/mqtt-manager.git;protocol=https;branch=main; \
    file://mqttmanager.service \
    file://mqttmanager.conf \
    "

SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

DEPENDS = "glib-2.0 \
           mosquitto \
           systemd"

inherit deploy cmake pkgconfig systemd
SYSTEMD_SERVICE_${PN} = "mqttmanager.service"

do_install_append() {
    rm -rf "${D}/etc/smartlamp.conf"
    rm -rf "${D}/etc/systemd/system/mqttmanager.service"
    
    install -d 0744 "${D}/etc"
    install -m 0644 "${WORKDIR}/mqttmanager.conf" "${D}/etc/mqttmanager.conf"
    
    install -d 0744 "${D}/etc/systemd/system"
    install -m 0644 "${WORKDIR}/mqttmanager.service" "${D}/etc/systemd/system/mqttmanager.service"
}

FILES_${PN} += "/usr/share/licenses/mqtt-manager \
                /usr/share/licenses/mqtt-manager/LICENSE \
                /etc \
                /etc/mqttmanager.conf \
                /etc/systemd \
                /etc/systemd/system \
                /etc/systemd/system/mqttmanager.service \
               "
