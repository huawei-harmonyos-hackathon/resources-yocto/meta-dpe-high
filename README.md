This README file contains information on the contents of the meta-dpe-high layer.    
Please see the corresponding sections below for details.

## Dependencies
Meta Openembedded:
* URI: http://cgit.openembedded.org/meta-openembedded        
* branch: dunfell    

## Patches
Please submit any patches against the meta-dpe-high layer please create a merge request for this project
* URI: https://gitlab.com/harmonyos-hackathon/resources-yocto/meta-dpe-high    
* Maintainer: Alin Popa (alin.popa.ext@huawei.com)

## Table of Contents
I. Adding the meta-dpe-high layer to your build    
II. Build a demo

### I. Adding the meta-dpe-high layer to your build
1. Clone this meta layer into your workspace      
```
# git clone https://gitlab.com/harmonyos-hackathon/resources-yocto/meta-dpe-high.git
```

2. Source Yocto build environment
```
# source poky/oe-init-build-env ohos-build
```

3. Add the layer in your layer config
```
# vim conf/bblayers.conf
# <add meta-dpe-high as any other layers from sources directory>
```

4. Add packages you need from this meta-layer to your conf/local.conf

### II. Build a demo
A demo target can be built by specifying the MACHINE and adding the needed recipes to the target.    
Eg. Smart Lamp Demo: https://gitlab.com/harmonyos-hackathon/resources-yocto/smart-lamp-demo

```
# MACHINE=raspberrypi4-64 bitbake core-image-minimal
```

